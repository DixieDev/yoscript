use yo::script::{OPS, USIZE_SIZE, OpParam};
use std::convert::TryInto;
use std::io::Read;

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() != 2 {
        eprintln!("Incorrect number of parameters!\nUsage: yo [bytecode_file]");
        return;
    }

    let mut file = std::fs::File::open(&args[1]).unwrap();
    let mut data: Vec<u8> = Vec::new();
    file.read_to_end(&mut data).unwrap();

    let mut found_valid_op = false;
    let mut instruction = 0;
    while instruction < data.len() {

        print!("{:#06x}: ", instruction);
        let byte = data[instruction];
        if byte as usize >= OPS.len() {
            println!("{:#04x}", byte);
            instruction += 1;
            continue;
        }

        found_valid_op = true;
        let op = OPS[byte as usize];
        instruction += 1;

        match op.param {
            OpParam::None => println!("{:?}", op.op),
            OpParam::Usize => {
                if instruction + USIZE_SIZE >= data.len() {
                    println!("{:#04x}", byte);
                } else {
                    let param = usize::from_le_bytes(data[instruction..instruction+USIZE_SIZE].try_into().unwrap());
                    println!("{:?} {:#010x}", op.op, param);
                    instruction += op.param.size();
                }
            },
            OpParam::U8 => {
                if instruction + USIZE_SIZE >= data.len() {
                    println!("{:#04x}", byte);
                } else {
                    let param = u8::from_le_bytes(data[instruction..instruction+1].try_into().unwrap());
                    println!("{:?} {}", op.op, param);
                    instruction += op.param.size();
                }
            },
            OpParam::U32 => {
                if instruction + USIZE_SIZE >= data.len() {
                    println!("{:#04x}", byte);
                } else {
                    let param = u32::from_le_bytes(data[instruction..instruction+4].try_into().unwrap());
                    println!("{:?} {}", op.op, param);
                    instruction += op.param.size();
                }
            },
            OpParam::I8 => {
                if instruction + USIZE_SIZE >= data.len() {
                    println!("{:#04x}", byte);
                } else {
                    let param = i8::from_le_bytes(data[instruction..instruction+1].try_into().unwrap());
                    println!("{:?} {}", op.op, param);
                    instruction += op.param.size();
                }
            },
            OpParam::I32 => {
                if instruction + USIZE_SIZE >= data.len() {
                    println!("{:#04x}", byte);
                } else {
                    let param = i32::from_le_bytes(data[instruction..instruction+4].try_into().unwrap());
                    println!("{:?} {}", op.op, param);
                    instruction += op.param.size();
                }
            },
        }
    }

    if !found_valid_op {
        eprintln!("No valid opcodes found in provided bytecode file!");
    }
}

