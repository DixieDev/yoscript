mod size_types;

use std::ops::{ Add, AddAssign, Sub, SubAssign };
pub use size_types::{ToByteSize, Bytes, Kilobytes, Megabytes};

#[derive(Copy, Clone, Debug)]
pub struct YoAddress(pub usize);

impl Add<usize> for YoAddress {
    type Output=Self;
    fn add(self, rhs: usize) -> Self::Output {
        YoAddress(self.0 + rhs)
    }
}

impl AddAssign<usize> for YoAddress {
    fn add_assign(&mut self, rhs: usize) {
        self.0 += rhs;
    }
}

impl Sub<usize> for YoAddress {
    type Output=Self;
    fn sub(self, rhs: usize) -> Self::Output {
        YoAddress(self.0 - rhs)
    }
}

impl SubAssign<usize> for YoAddress {
    fn sub_assign(&mut self, rhs: usize) {
        self.0 -= rhs;
    }
}

pub type YoMemoryResult<T=()> = Result<T, YoMemoryError>;

#[derive(Debug)]
pub enum YoMemoryError {
    OutOfBounds(YoAddress),
    ProtectedMemoryAccess(YoAddress),
    RangeProtectedMemoryAccess {
        start: YoAddress,
        len: usize,
    },
    RangeOutOfBounds {
        start: YoAddress, 
        len: usize,
    },
    UnallocatedFree {
        start: YoAddress,
        free_len: usize,
    }
}

pub struct YoPartitionConfig<S, C, H> 
where S: ToByteSize,
      C: ToByteSize,
      H: ToByteSize,
{
    pub stack_size: S,
    pub code_size: C,
    pub heap_size: H,
}

pub struct YoMemory {
    descriptor: PartitioningDescriptor,
    bytes: Vec<u8>,

    stack_head: usize,
    code_head: usize,
    heap_head: usize,
}

impl YoMemory {
    pub fn new<S, C, H>(config: YoPartitionConfig<S, C, H>) -> Self 
    where S: ToByteSize,
          C: ToByteSize,
          H: ToByteSize,
    {
        let (stack_size, code_size, heap_size) = (
            config.stack_size.to_byte_size().0,
            config.code_size.to_byte_size().0,
            config.heap_size.to_byte_size().0,
        );

        let total_size = stack_size + code_size + heap_size;

        Self {
            descriptor: PartitioningDescriptor {
                stack_size,
                code_size,
                heap_size,
            },

            bytes: vec![0_u8; total_size],

            stack_head: 0,
            code_head: 0,
            heap_head: 0,
        }
    }

    pub fn size(&self) -> usize {
        self.bytes.len()
    }

    pub fn write_byte(&mut self, addr: YoAddress, byte: u8) -> YoMemoryResult {
        let stack_addr = self.descriptor.stack_address().0;
        let in_stack_bounds = addr.0 >= stack_addr && addr.0 < stack_addr+self.stack_head; 

        let heap_addr = self.descriptor.heap_address().0;
        let in_heap_bounds = addr.0 >= heap_addr && addr.0 < heap_addr+self.heap_head;

        if in_stack_bounds || in_heap_bounds {
            self.bytes[addr.0] = byte;
            Ok(())
        } else {
            Err(YoMemoryError::ProtectedMemoryAccess(addr))
        }
    }

    pub fn write_bytes(&mut self, start_addr: YoAddress, bytes: &[u8]) -> YoMemoryResult {
        let start = start_addr.0;
        let end = start_addr.0 + bytes.len();

        let stack_start = self.descriptor.stack_address().0;
        let stack_end = stack_start + self.stack_head;
        let in_stack_bounds = start >= stack_start && end <= stack_end; 

        let heap_start = self.descriptor.heap_address().0;
        let heap_end = heap_start + self.heap_head;
        let in_heap_bounds = start >= heap_start && end <= heap_end;

        if in_stack_bounds || in_heap_bounds {
            self.bytes[start..end].copy_from_slice(bytes);
            Ok(())
        } else {
            Err(YoMemoryError::RangeProtectedMemoryAccess{
                start: start_addr,
                len: bytes.len(),
            })
        }
    }

    pub fn get_byte(&self, addr: YoAddress) -> YoMemoryResult<u8> {
        if addr.0 < self.bytes.len() {
            Ok(self.bytes[addr.0])
        } else {
            Err(YoMemoryError::OutOfBounds(addr))
        }
    }

    pub fn get_slice(&self, start: YoAddress, len: usize) -> YoMemoryResult<&[u8]>{
        if start.0 + len > self.bytes.len() {
            Err(YoMemoryError::RangeOutOfBounds {start, len })
        } else {
            Ok(&self.bytes[start.0..start.0+len])
        }
    }

    fn copy_into(&mut self, start: YoAddress, data: &[u8]) -> YoMemoryResult {
        if start.0 + data.len() > self.bytes.len() {
            Err(YoMemoryError::RangeOutOfBounds {
                start,
                len: data.len(),
            })
        } else {
            self.bytes[start.0..start.0+data.len()].copy_from_slice(data);
            Ok(())
        }
    }

    pub fn copy_into_code(&mut self, start: YoAddress, data: &[u8]) -> YoMemoryResult {
        self.copy_into(start, data)
    }

    pub fn push_code(&mut self, data: &[u8]) -> YoMemoryResult<YoAddress> {
        let start = self.code_head();
        self.copy_into(start, data)?;

        let result = self.code_head;
        self.code_head += data.len();
        Ok(YoAddress(result))
    }

    pub fn pop_code(&mut self, len: usize) -> YoMemoryResult {
        if self.code_head >= len {
            self.code_head -= len;
            Ok(())
        } else {
            Err(YoMemoryError::UnallocatedFree {
                start: self.code_head(),
                free_len: len,
            })
        }
    }

    pub fn clear_code(&mut self) {
        self.code_head = 0;
    }

    pub fn push_stack(&mut self, len: usize) -> YoMemoryResult<YoAddress> {
        let start = self.stack_head();
        if start.0 + len <= self.descriptor.stack_address().0 + self.descriptor.stack_size {
            let result = self.stack_head();
            self.stack_head += len;
            Ok(result)
        } else {
            Err(YoMemoryError::RangeOutOfBounds {
                start,
                len: len,
            })
        }
    }

    pub fn pop_stack(&mut self, len: usize) -> YoMemoryResult {
        if self.stack_head >= len {
            self.stack_head -= len;
            Ok(())
        } else {
            Err(YoMemoryError::UnallocatedFree {
                start: self.stack_head(),
                free_len: len,
            })
        }
    }

    pub fn clear_stack(&mut self) {
        self.stack_head = 0;
    }

    pub fn print_stack_range(&self, range: std::ops::Range<usize>) {
        if range.start >= self.descriptor.stack_size {
            println!("Stack ({:#x} to {:#x}): [ out of bounds ]", range.start, range.end);
        } else {
            let mut out = format!("Stack ({:#x} to {:#x}): [ \n    ", range.start, range.end);
            for i in range.start..range.end - 1 {
                if i >= self.descriptor.stack_size {
                    out.push_str(&format!("out of bounds from {:#x} ]", i));
                    break;
                } else {
                    let mem_idx = self.descriptor.stack_address().0 + i;
                    out.push_str(&format!("{:02x} ", self.bytes[mem_idx]));
                }

                if (i+1) % 4 == 0 {
                    out.push_str("\n    ");
                }
            }

            if range.end-1 >= self.descriptor.stack_size {
                out.push_str(&format!("out of bounds from {:#x} ]", range.end-1));
            } else {
                let mem_idx = self.descriptor.stack_address().0 + range.end-1;
                out.push_str(&format!("{:02x}\n]", self.bytes[mem_idx]));
            }

            println!("{}", out);
        }
    }

    #[inline]
    pub fn stack(&self) -> &[u8] {
        let start = self.descriptor.stack_address().0;
        let end = start + self.descriptor.stack_size;
        &self.bytes[start..end]
    }

    #[inline]
    pub fn code(&self) -> &[u8] {
        let start = self.descriptor.code_address().0;
        let end = start + self.descriptor.code_size;
        &self.bytes[start..end]
    }

    #[inline]
    pub fn code_head(&self) -> YoAddress {
        YoAddress(self.descriptor.code_address().0 + self.code_head)
    }

    #[inline]
    pub fn stack_head(&self) -> YoAddress {
        YoAddress(self.descriptor.stack_address().0 + self.stack_head)
    }

    #[inline]
    pub fn set_stack_head(&mut self, addr: YoAddress) -> YoMemoryResult {
        let head = addr.0 - self.descriptor.stack_address().0;
        if head > self.descriptor.stack_size {
            Err(YoMemoryError::OutOfBounds(addr))
        } else {
            self.stack_head = head;
            Ok(())
        }
    }

    #[inline]
    pub fn heap_head(&self) -> YoAddress {
        YoAddress(self.descriptor.heap_address().0 + self.heap_head)
    }

    #[inline]
    pub fn stack_usage(&self) -> usize {
        self.stack_head
    }

    #[inline]
    pub fn stack_size(&self) -> usize {
        self.descriptor.stack_size
    }

    #[inline]
    pub fn heap_usage(&self) -> usize {
        self.heap_head
    }

    #[inline]
    pub fn heap_size(&self) -> usize {
        self.descriptor.heap_size
    }

    #[inline]
    pub fn code_usage(&self) -> usize {
        self.code_head
    }

    #[inline]
    pub fn code_size(&self) -> usize {
        self.descriptor.code_size
    }
}

impl std::fmt::Debug for YoMemory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "YoMemory {{\n\tstack: {} / {} bytes,\n\tcode:  {} / {} bytes,\n\theap:  {} / {} bytes,\n}}",
            self.stack_head, self.descriptor.stack_size,
            self.code_head, self.descriptor.code_size,
            self.heap_head, self.descriptor.heap_size,
        )
    }
}

#[derive(Debug)]
struct PartitioningDescriptor {
    stack_size: usize,
    code_size: usize,
    heap_size: usize,
}

impl PartitioningDescriptor {
    #[inline]
    fn code_address(&self) -> YoAddress {
        YoAddress(0)
    }

    #[inline]
    fn stack_address(&self) -> YoAddress {
        YoAddress(self.code_size)
    }

    #[inline]
    fn heap_address(&self) -> YoAddress {
        YoAddress(self.code_size + self.stack_size)
    }
}
