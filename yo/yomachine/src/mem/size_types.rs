pub trait ToByteSize {
    fn to_byte_size(self) -> Bytes;
}

#[derive(Copy, Clone, Debug)] pub struct Bytes(pub usize);
#[derive(Copy, Clone, Debug)] pub struct Kilobytes(pub usize);
#[derive(Copy, Clone, Debug)] pub struct Megabytes(pub usize);

impl ToByteSize for Bytes {
    #[inline] fn to_byte_size(self) -> Bytes { self }
}

impl ToByteSize for Kilobytes { 
    #[inline] fn to_byte_size(self) -> Bytes { Bytes(self.0 * 1024) }
}

impl ToByteSize for Megabytes { 
    #[inline] fn to_byte_size(self) -> Bytes { Bytes(self.0 * 1024 * 1024) }
}
