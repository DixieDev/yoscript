use std::str::FromStr;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hasher, Hash};
use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};
use yoscript::{YoOp, OpParam};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum TokenType {
    None,
    Op,
    LabelRef,
    LabelDef,
    Literal,
}

#[derive(Debug)]
enum Token {
    Op(YoOp),
    LabelDef(String),
    LabelRef(String),
    Num,
}

impl Token {
    fn tok_type(&self) -> TokenType {
        match self {
            Self::Op(_) => TokenType::Op,
            Self::LabelRef(_) => TokenType::LabelRef,
            Self::LabelDef(_) => TokenType::LabelDef,
            Self::Num => TokenType::Literal,
        }
    }
}

fn token(s: &str) -> Option<Token> {
    if s.is_empty() {
        return None;
    }

    match s {
        "nop" => Some(Token::Op(YoOp::NoOp)),
        "push" => Some(Token::Op(YoOp::Push)),
        "pop" => Some(Token::Op(YoOp::Pop)),
        "alloc" => Some(Token::Op(YoOp::Alloc)),
        "free" => Some(Token::Op(YoOp::Free)),
        "store8" => Some(Token::Op(YoOp::Store8)),
        "store32" => Some(Token::Op(YoOp::Store32)),
        "storeusize" => Some(Token::Op(YoOp::StoreUsize)),
        "load8" => Some(Token::Op(YoOp::Load8)),
        "load32" => Some(Token::Op(YoOp::Load32)),
        "deref8" => Some(Token::Op(YoOp::Load8De)),
        "deref32" => Some(Token::Op(YoOp::Load32De)),
        "loadptr" => Some(Token::Op(YoOp::LoadPtr)),
        "derefaddr" => Some(Token::Op(YoOp::LoadPtrDe)),
        "loadsp" => Some(Token::Op(YoOp::LoadStackPtr)),
        "loadsprel" => Some(Token::Op(YoOp::LoadStackPtrRel)),
        "addptr" => Some(Token::Op(YoOp::AddPtr)),
        "subptr" => Some(Token::Op(YoOp::SubPtr)),
        "exit" => Some(Token::Op(YoOp::Exit)),
        "store8acc" => Some(Token::Op(YoOp::StoreAcc8)),
        "store32acc" => Some(Token::Op(YoOp::StoreAcc32)),
        "cmpu8" => Some(Token::Op(YoOp::CmpU8)),
        "cmpu32" => Some(Token::Op(YoOp::CmpU32)),
        "cmpi8" => Some(Token::Op(YoOp::CmpI8)),
        "cmpi32" => Some(Token::Op(YoOp::CmpI32)),
        "cmpu8de" => Some(Token::Op(YoOp::CmpU8De)),
        "cmpu32de" => Some(Token::Op(YoOp::CmpU32De)),
        "cmpi8de" => Some(Token::Op(YoOp::CmpI8De)),
        "cmpi32de" => Some(Token::Op(YoOp::CmpI32De)),
        "cmpptr" => Some(Token::Op(YoOp::CmpPtr)),
        "cmpptrde" => Some(Token::Op(YoOp::CmpPtrDe)),
        "jmp" => Some(Token::Op(YoOp::Jump)),
        "jmpeq" => Some(Token::Op(YoOp::JumpEq)),
        "jmpne" => Some(Token::Op(YoOp::JumpNe)),
        "jmpgt" => Some(Token::Op(YoOp::JumpGt)),
        "jmplt" => Some(Token::Op(YoOp::JumpLt)),
        "jmpge" => Some(Token::Op(YoOp::JumpGe)),
        "jmple" => Some(Token::Op(YoOp::JumpLe)),
        "call" => Some(Token::Op(YoOp::Call)),
        "callptr" => Some(Token::Op(YoOp::CallPtr)),
        "ret" => Some(Token::Op(YoOp::Return)),
        "addu8de" => Some(Token::Op(YoOp::AddU8De)),
        "addu32de" => Some(Token::Op(YoOp::AddU32De)),
        "addi8de" => Some(Token::Op(YoOp::AddI8De)),
        "addi32de" => Some(Token::Op(YoOp::AddI32De)),
        "subu8de" => Some(Token::Op(YoOp::SubU8De)),
        "subu32de" => Some(Token::Op(YoOp::SubU32De)),
        "subi8de" => Some(Token::Op(YoOp::SubI8De)),
        "subi32de" => Some(Token::Op(YoOp::SubI32De)),
        "mulu8de" => Some(Token::Op(YoOp::MulU8De)),
        "mulu32de" => Some(Token::Op(YoOp::MulU32De)),
        "muli8de" => Some(Token::Op(YoOp::MulI8De)),
        "muli32de" => Some(Token::Op(YoOp::MulI32De)),
        "divu8de" => Some(Token::Op(YoOp::DivU8De)),
        "divu32de" => Some(Token::Op(YoOp::DivU32De)),
        "divi8de" => Some(Token::Op(YoOp::DivI8De)),
        "divi32de" => Some(Token::Op(YoOp::DivI32De)),
        "print" => Some(Token::Op(YoOp::Print)),
        "printu32" => Some(Token::Op(YoOp::PrintU32)),
        "printi32" => Some(Token::Op(YoOp::PrintI32)),
        "zeroacc" => Some(Token::Op(YoOp::ZeroAcc)),
        "pushbp" => Some(Token::Op(YoOp::PushBasePtr)),
        "popbp" => Some(Token::Op(YoOp::PopBasePtr)),
        "loadbp" => Some(Token::Op(YoOp::LoadBasePtr)),
        "loadbprel" => Some(Token::Op(YoOp::LoadBasePtrRel)),
        "not" => Some(Token::Op(YoOp::Not)),
        s => {
            let first_char = s.chars().nth(0).unwrap();
            let is_label_char = first_char.is_alphabetic() || first_char == '_';
            if is_label_char {
                if s.ends_with(":") {
                    Some(Token::LabelDef(s[0..s.len()-1].to_owned()))
                } else {
                    Some(Token::LabelRef(s[0..s.len()].to_owned()))
                }
            } else {
                for (i, c) in s.chars().enumerate() {
                    if !c.is_numeric() && (c !=  '-' || i > 0) {
                        return None;
                    }
                }
                Some(Token::Num)
            }
        }
    }
}

pub struct YoAssembly {
    code: Vec<u8>,
}

impl YoAssembly {
    pub fn code(&self) -> &[u8] {
        &self.code
    }
}

pub struct YoAssembler {
    code: Vec<u8>,
    labels: Vec<(u64, String, usize)>,
    label_refs: Vec<(u64, String, usize)>,
}

impl YoAssembler {
    pub fn assemble_file<P: AsRef<Path>>(path: P) -> YoAssembly {
        let mut asm = Self {
            code: Vec::new(),
            labels: Vec::new(),
            label_refs: Vec::new(),
        };

        let mut reader = BufReader::new(File::open(path).unwrap());
        let mut line = String::new();
        loop {
            match reader.read_line(&mut line) {
                Ok(0) => break,
                Ok(_) => { 
                    asm.assemble_line(&line);
                    line.clear();
                },
                Err(e) => panic!("{:?}", e),
            }
        }

        asm.link_labels();

        YoAssembly {
            code: asm.code,
        }
    }

    fn link_labels(&mut self) {
        let mut unused_labels = vec![true; self.labels.len()];
        for ref_idx in 0..self.label_refs.len() {
            let mut resolved_ref = false;
            for label_idx in 0..self.labels.len() {
                if self.label_refs[ref_idx].0 == self.labels[label_idx].0
                    && self.label_refs[ref_idx].1 == self.labels[label_idx].1 
                {
                    resolved_ref = true;
                    unused_labels[label_idx] = false;
                    let overwrite_addr = self.label_refs[ref_idx].2;
                    let value = self.labels[label_idx].2;
                    let bytes = value.to_le_bytes();
                    self.code[overwrite_addr..overwrite_addr+bytes.len()].copy_from_slice(&bytes);
                }
            }

            if !resolved_ref {
                panic!("No label '{}' found", self.label_refs[ref_idx].2);
            }
        }
    }

    fn assemble_line(&mut self, line: &str) {
        let toks = {
            let mut toks = Vec::new();
            let mut tok = String::new();
            for c in line.chars() {
                if c.is_whitespace() {
                    if !tok.is_empty() {
                        toks.push(tok.clone().to_lowercase());
                        tok.clear();
                    }
                } else if c == ';' {
                    break;
                } else {
                    tok.push(c);
                }
            }

            if !tok.is_empty() {
                toks.push(tok.clone().to_lowercase());
                tok.clear();
            }

            toks
        };

        let mut expect: &[TokenType] = &[TokenType::Op, TokenType::LabelDef, TokenType::None];
        let mut next_param = None;
        for tok_str in toks.iter() {
            let tok = token(tok_str);
            match tok {
                Some(tok) => {
                    if !expect.contains(&tok.tok_type()) {
                        panic!("Expected token of type {:?}, but got {:?} on line:\n{}", expect, tok.tok_type(), line); 
                    }
                    match tok {
                        Token::Op(op) => {
                            let info = op.info();
                            self.code.push(op as u8);
                            match info.param {
                                OpParam::None => expect = &[TokenType::None],
                                OpParam::Usize => {
                                    expect = &[TokenType::Literal, TokenType::LabelRef];
                                    next_param = Some(info.param);
                                },
                                _ => { 
                                    expect = &[TokenType::Literal];
                                    next_param = Some(info.param);
                                }
                            }
                        },
                        Token::LabelRef(label) => {
                            let hash = {
                                let mut hasher = DefaultHasher::new();
                                label.hash(&mut hasher);
                                hasher.finish()
                            };

                            self.label_refs.push((hash, label.clone(), self.code.len()));
                            self.code.extend_from_slice(&usize::to_le_bytes(0));

                            expect = &[TokenType::None];
                        }
                        Token::LabelDef(label) => {
                            let hash = {
                                let mut hasher = DefaultHasher::new();
                                label.hash(&mut hasher);
                                hasher.finish()
                            };

                            for l in self.labels.iter() {
                                if l.0 == hash && l.1 == label {
                                    panic!("Label '{}' specified multiple times!!!", label);
                                } 
                            }

                            self.labels.push((hash, label.clone(), self.code.len()));
                        },
                        Token::Num => {
                            match next_param {
                                Some(OpParam::U8) => {
                                    let num = if tok_str.starts_with("-") {
                                        let num = i8::from_str(tok_str).unwrap();
                                        u8::from_le_bytes(num.to_le_bytes())
                                    } else {
                                        u8::from_str(tok_str).unwrap()
                                    };
                                    self.code.push(num);
                                },
                                Some(OpParam::U32) => {
                                    let num = if tok_str.starts_with("-") {
                                        let num = i32::from_str(tok_str).unwrap();
                                        u32::from_le_bytes(num.to_le_bytes())
                                    } else {
                                        u32::from_str(tok_str).unwrap()
                                    };
                                    self.code.extend_from_slice(&num.to_le_bytes());
                                },
                                Some(OpParam::I8) => {
                                    let num = i8::from_str(tok_str).unwrap();
                                    self.code.push(num.to_le_bytes()[0]);
                                },
                                Some(OpParam::I32) => {
                                    let num = i32::from_str(tok_str).unwrap();
                                    self.code.extend_from_slice(&num.to_le_bytes());
                                },
                                Some(OpParam::Usize) => {
                                    let num = usize::from_str(tok_str).unwrap();
                                    self.code.extend_from_slice(&num.to_le_bytes());
                                },
                                Some(OpParam::None) => unreachable!("WTF!!! Next param is somehow expected to be None"),
                                None => unreachable!("WTF! Next param is none, but the current token {} is a number!!", tok_str),
                            }

                            expect = &[TokenType::None];
                            next_param = None;
                        }
                    }
                },
                None => panic!("'{}' is not a valid token", tok_str),
            }
        }

        if !expect.contains(&TokenType::None) {
            panic!("Unexpected end of line. Expected token of type {:?} on line:\n{}", expect, line);
        }
    }
}
