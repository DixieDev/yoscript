use std::convert::TryInto;
use yoscript::{ OPS, USIZE_SIZE, YoOp, OpInfo };
use crate::mem::{
    ToByteSize,
    YoMemory,
    YoAddress,
    YoPartitionConfig,
};

struct YoFlags {
    gt: bool,
    lt: bool,
    eq: bool,
}

struct YoRegisters {
    accumulator: [u8; 4],
    ptr: YoAddress,
    instruction_ptr: YoAddress,
    base_ptr: YoAddress,
}

pub struct YoMachine {
    regs: YoRegisters,
    flags: YoFlags,
    pub mem: YoMemory,
}

impl std::fmt::Debug for YoMachine {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f, 
r##"YoMachine {{
    registers: YoRegisters {{
        acc: [ {:#x}, {:#x}, {:#x}, {:#x} ],
        ptr: {:#x},
        instruction_ptr: {:#x},
        base_ptr: {:#x},
    }},
    flags: YoFlags {{
        eq: {},
        gt: {},
        lt: {},
    }},
    mem: YoMemory {{
        code_usage : {} / {} bytes
        stack_usage: {} / {} bytes
        heap_usage : {} / {} bytes
    }}
}}"##,
            self.regs.accumulator[0], self.regs.accumulator[1], self.regs.accumulator[2], self.regs.accumulator[3], 
            self.regs.ptr.0,
            self.regs.instruction_ptr.0,
            self.regs.base_ptr.0,

            self.flags.eq,
            self.flags.gt,
            self.flags.lt,

            self.mem.code_usage(), self.mem.code_size(),
            self.mem.stack_usage(), self.mem.stack_size(),
            self.mem.heap_usage(), self.mem.heap_size(),
        )
    }
}

pub enum YoExecProgress {
    OnGoing,
    Completed,
}

impl YoMachine {
    pub fn new<S, C, H>(partition_config: YoPartitionConfig<S, C, H>) -> Self 
    where S: ToByteSize,
          C: ToByteSize,
          H: ToByteSize,
    {
        let mem = YoMemory::new(partition_config);
        let stack_addr = mem.stack_head();
        Self {
            mem,
            regs: YoRegisters {
                accumulator: [0; 4],
                ptr: YoAddress(0),
                instruction_ptr: YoAddress(0),
                base_ptr: stack_addr,
            },
            flags: YoFlags {
                gt: false,
                lt: false,
                eq: false,
            },
        }
    }

    pub fn reset_exec(&mut self) {
        self.mem.clear_stack();

        self.regs = YoRegisters {
            accumulator: [0; 4],
            ptr: YoAddress(0),
            instruction_ptr: YoAddress(0),
            base_ptr: YoAddress(0),
        };

        self.flags = YoFlags {
            gt: false,
            lt: false,
            eq: false,
        };
    }

    #[inline]
    fn decode_next_op(&mut self) -> Option<OpInfo> {
        let code = self.mem.code();
        let instr_ptr = self.regs.instruction_ptr.0;
        if instr_ptr < code.len() {
            let op = code[instr_ptr] as usize;
            if op < OPS.len() {
                self.regs.instruction_ptr.0 += 1;
                Some(OPS[op])
            } else {
                None
            }
        } else {
            None
        }
    }

    #[inline]
    fn decode_u8_param(&mut self) -> Option<u8> {
        let code = self.mem.code();
        let instr_ptr = self.regs.instruction_ptr.0;
        if instr_ptr < code.len() {
            self.regs.instruction_ptr.0 += 1;
            Some(code[instr_ptr])
        } else {
            None
        }
    }

    #[inline]
    fn decode_u32_param(&mut self) -> Option<u32> {
        let code = self.mem.code();
        let instr_ptr = self.regs.instruction_ptr.0;
        if instr_ptr+4 < code.len() {
            self.regs.instruction_ptr.0 += 4;
            Some(u32::from_le_bytes(code[instr_ptr..instr_ptr+4].try_into().unwrap()))
        } else {
            None
        }
    }

    #[inline]
    fn decode_i8_param(&mut self) -> Option<i8> {
        let code = self.mem.code();
        let instr_ptr = self.regs.instruction_ptr.0;
        if instr_ptr < code.len() {
            self.regs.instruction_ptr.0 += 1;
            Some(i8::from_le_bytes([code[instr_ptr]]))
        } else {
            None
        }
    }

    #[inline]
    fn decode_i32_param(&mut self) -> Option<i32> {
        let code = self.mem.code();
        let instr_ptr = self.regs.instruction_ptr.0;
        if instr_ptr+4 < code.len() {
            self.regs.instruction_ptr.0 += 4;
            Some(i32::from_le_bytes(code[instr_ptr..instr_ptr+4].try_into().unwrap()))
        } else {
            None
        }
    }

    #[inline]
    fn decode_usize_param(&mut self) -> Option<usize> {
        let code = self.mem.code();
        let instr_ptr = self.regs.instruction_ptr.0;
        if instr_ptr+USIZE_SIZE < code.len() {
            self.regs.instruction_ptr.0 += USIZE_SIZE;
            Some(usize::from_le_bytes(code[instr_ptr..instr_ptr+USIZE_SIZE].try_into().unwrap()))
        } else {
            None
        }
    }

    #[inline]
    fn deref_u8(&self, addr: YoAddress) -> u8 {
        self.mem.get_byte(addr).unwrap()
    }

    #[inline]
    fn deref_u32(&self, addr: YoAddress) -> u32 {
        let slice = self.mem.get_slice(addr, 4).unwrap();
        u32::from_le_bytes(slice.try_into().unwrap())
    }

    #[inline]
    fn deref_i8(&self, addr: YoAddress) -> i8 {
        i8::from_le_bytes([self.mem.get_byte(addr).unwrap()])
    }

    #[inline]
    fn deref_i32(&self, addr: YoAddress) -> i32 {
        let slice = self.mem.get_slice(addr, 4).unwrap();
        i32::from_le_bytes(slice.try_into().unwrap())
    }

    #[inline]
    fn deref_usize(&self, addr: YoAddress) -> usize {
        let slice = self.mem.get_slice(addr, USIZE_SIZE).unwrap();
        usize::from_le_bytes(slice.try_into().unwrap())
    }

    #[inline]
    fn exec(&mut self, op: YoOp) {
        match op {
            YoOp::NoOp => (),
            YoOp::Exit => (),
            YoOp::Push => {
                let param = self.decode_usize_param().unwrap();
                self.mem.push_stack(param).unwrap(); 
            }
            YoOp::Pop => {
                let param = self.decode_usize_param().unwrap();
                self.mem.pop_stack(param).unwrap(); 
            },
            YoOp::Alloc => {
                let _param = self.decode_usize_param().unwrap();
                eprintln!("Alloc not yet implemented");
            },
            YoOp::Free => {
                let _param = self.decode_usize_param().unwrap();
                eprintln!("Free not yet implemented");
            },
            YoOp::Store8 => {
                let param = self.decode_u8_param().unwrap();
                self.mem.write_byte(self.regs.ptr, param).unwrap();
            },
            YoOp::Store32 => {
                let param = self.decode_u32_param().unwrap();
                self.mem.write_bytes(self.regs.ptr, &param.to_le_bytes()).expect(&format!("Failed to write {} to {:#x?}", param, self.regs.ptr));
            },
            YoOp::StoreUsize => {
                let param = self.decode_usize_param().unwrap();
                self.mem.write_bytes(self.regs.ptr, &param.to_le_bytes()).unwrap();
            },
            YoOp::StoreAcc8 => self.mem.write_byte(self.regs.ptr, self.regs.accumulator[0]).unwrap(),
            YoOp::StoreAcc32 => self.mem.write_bytes(self.regs.ptr, &self.regs.accumulator).unwrap(),
            YoOp::Load8 => {
                let param = self.decode_u8_param().unwrap();
                self.regs.accumulator[0] = param;
            },
            YoOp::Load32 => {
                let param = self.decode_u32_param().unwrap();
                self.regs.accumulator = param.to_le_bytes();
            },
            YoOp::Load8De => self.regs.accumulator[0] = self.deref_u8(self.regs.ptr),
            YoOp::Load32De => self.regs.accumulator = self.deref_u32(self.regs.ptr).to_le_bytes(),
            YoOp::LoadPtr => {
                let param = self.decode_usize_param().unwrap();
                self.regs.ptr = YoAddress(param);
            },
            YoOp::LoadPtrDe => self.regs.ptr = YoAddress(self.deref_usize(self.regs.ptr)),
            YoOp::LoadStackPtr => self.regs.ptr = self.mem.stack_head(),
            YoOp::LoadStackPtrRel => {
                let param = self.decode_usize_param().unwrap();
                self.regs.ptr.0 = self.mem.stack_head().0-param;
            },
            YoOp::AddPtr => {
                let param = self.decode_usize_param().unwrap();
                self.regs.ptr.0 += param;
            },
            YoOp::SubPtr => {
                let param = self.decode_usize_param().unwrap();
                self.regs.ptr.0 -= param;
            },
            YoOp::CmpU8 => {
                let param = self.decode_u8_param().unwrap();
                self.flags.eq = self.regs.accumulator[0] == param;
                self.flags.gt = self.regs.accumulator[0] > param;
                self.flags.lt = self.regs.accumulator[0] < param;
            }, 
            YoOp::CmpU32 => { 
                let param = self.decode_u32_param().unwrap();
                let acc = u32::from_le_bytes(self.regs.accumulator);
                self.flags.eq = acc == param;
                self.flags.gt = acc > param;
                self.flags.lt = acc < param;
            },
            YoOp::CmpI8 => {
                let param = self.decode_i8_param().unwrap();
                let acc = i8::from_le_bytes([self.regs.accumulator[0]]);
                self.flags.eq = acc == param;
                self.flags.gt = acc > param;
                self.flags.lt = acc < param;
            }, 
            YoOp::CmpI32 => { 
                let param = self.decode_i32_param().unwrap();
                let acc = i32::from_le_bytes(self.regs.accumulator);
                self.flags.eq = acc == param;
                self.flags.gt = acc > param;
                self.flags.lt = acc < param;
            },
            YoOp::CmpU8De => {
                let byte = self.deref_u8(self.regs.ptr);
                self.flags.eq = self.regs.accumulator[0] == byte;
                self.flags.gt = self.regs.accumulator[0] > byte;
                self.flags.lt = self.regs.accumulator[0] < byte;
            }, 
            YoOp::CmpU32De => {
                let acc = u32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_u32(self.regs.ptr);
                self.flags.eq = acc == num;
                self.flags.gt = acc > num;
                self.flags.lt = acc < num;
            }, 
            YoOp::CmpI8De => {
                let acc = i8::from_le_bytes([self.regs.accumulator[0]]);
                let byte = self.deref_i8(self.regs.ptr);
                self.flags.eq = acc == byte;
                self.flags.gt = acc > byte;
                self.flags.lt = acc < byte;
            }, 
            YoOp::CmpI32De => {
                let acc = i32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_i32(self.regs.ptr);
                self.flags.eq = acc == num;
                self.flags.gt = acc > num;
                self.flags.lt = acc < num;
            }, 
            YoOp::CmpPtr => {
                let param = self.decode_usize_param().unwrap();
                self.flags.eq = self.regs.ptr.0 == param;
                self.flags.gt = self.regs.ptr.0 > param;
                self.flags.lt = self.regs.ptr.0 < param;
            }, 
            YoOp::CmpPtrDe => {
                let param = self.decode_usize_param().unwrap();
                let dest = self.deref_usize(self.regs.ptr);
                self.flags.eq = dest == param;
                self.flags.gt = dest > param;
                self.flags.lt = dest < param;
            }, 
            YoOp::Jump => {
                let param = self.decode_usize_param().unwrap();
                self.regs.instruction_ptr.0 = param;
            },
            YoOp::JumpEq => {
                let param = self.decode_usize_param().unwrap();
                if self.flags.eq { self.regs.instruction_ptr.0 = param; }
            },
            YoOp::JumpNe => {
                let param = self.decode_usize_param().unwrap();
                if !self.flags.eq { self.regs.instruction_ptr.0 = param; }
            },
            YoOp::JumpGt => {
                let param = self.decode_usize_param().unwrap();
                if self.flags.gt { self.regs.instruction_ptr.0 = param; }
            },
            YoOp::JumpLt => {
                let param = self.decode_usize_param().unwrap();
                if self.flags.lt { self.regs.instruction_ptr.0 = param; }
            },
            YoOp::JumpGe => {
                let param = self.decode_usize_param().unwrap();
                if !self.flags.lt { self.regs.instruction_ptr.0 = param; }
            },
            YoOp::JumpLe => {
                let param = self.decode_usize_param().unwrap();
                if !self.flags.gt { self.regs.instruction_ptr.0 = param; }
            },
            YoOp::Call => {
                let param = self.decode_usize_param().unwrap();
                let addr = self.mem.push_stack(USIZE_SIZE).unwrap();
                self.mem.write_bytes(addr, &self.regs.instruction_ptr.0.to_le_bytes()).unwrap();
                self.regs.instruction_ptr.0 = param;
            },
            YoOp::CallPtr => {
                let addr = self.mem.push_stack(USIZE_SIZE).unwrap();
                self.mem.write_bytes(addr, &self.regs.instruction_ptr.0.to_le_bytes()).unwrap();
                self.regs.instruction_ptr = YoAddress(self.regs.ptr.0);
            },
            YoOp::Return => {
                let stack_head = self.mem.stack_head();
                self.regs.instruction_ptr = YoAddress(self.deref_usize(YoAddress(stack_head.0 - USIZE_SIZE)));
                self.mem.pop_stack(USIZE_SIZE).unwrap();
            },
            YoOp::AddU8De => self.regs.accumulator[0] += self.deref_u8(self.regs.ptr),
            YoOp::AddU32De => {
                let acc = u32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_u32(self.regs.ptr);
                self.regs.accumulator = (acc + num).to_le_bytes();
            },
            YoOp::AddI8De => {
                let acc = i8::from_le_bytes([self.regs.accumulator[0]]);
                let num = self.deref_i8(self.regs.ptr);
                self.regs.accumulator[0] = (acc + num).to_le_bytes()[0];
            },
            YoOp::AddI32De => {
                let acc = i32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_i32(self.regs.ptr);
                self.regs.accumulator = (acc + num).to_le_bytes();
            },
            YoOp::SubU8De => self.regs.accumulator[0] -= self.deref_u8(self.regs.ptr),
            YoOp::SubU32De => {
                let acc = u32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_u32(self.regs.ptr);
                self.regs.accumulator = (acc - num).to_le_bytes();
            },
            YoOp::SubI8De => {
                let acc = i8::from_le_bytes([self.regs.accumulator[0]]);
                let num = self.deref_i8(self.regs.ptr);
                self.regs.accumulator[0] = (acc - num).to_le_bytes()[0];
            },
            YoOp::SubI32De => {
                let acc = i32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_i32(self.regs.ptr);
                self.regs.accumulator = (acc - num).to_le_bytes();
            },
            YoOp::MulU8De => self.regs.accumulator[0] *= self.deref_u8(self.regs.ptr),
            YoOp::MulU32De => {
                let acc = u32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_u32(self.regs.ptr);
                self.regs.accumulator = (acc * num).to_le_bytes();
            },
            YoOp::MulI8De => {
                let acc = i8::from_le_bytes([self.regs.accumulator[0]]);
                let num = self.deref_i8(self.regs.ptr);
                self.regs.accumulator[0] = (acc * num).to_le_bytes()[0];
            },
            YoOp::MulI32De => {
                let acc = i32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_i32(self.regs.ptr);
                self.regs.accumulator = (acc * num).to_le_bytes();
            },
            YoOp::DivU8De => self.regs.accumulator[0] /= self.deref_u8(self.regs.ptr),
            YoOp::DivU32De => {
                let acc = u32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_u32(self.regs.ptr);
                self.regs.accumulator = (acc / num).to_le_bytes();
            },
            YoOp::DivI8De => {
                let acc = i8::from_le_bytes([self.regs.accumulator[0]]);
                let num = self.deref_i8(self.regs.ptr);
                self.regs.accumulator[0] = (acc / num).to_le_bytes()[0];
            },
            YoOp::DivI32De => {
                let acc = i32::from_le_bytes(self.regs.accumulator);
                let num = self.deref_i32(self.regs.ptr);
                self.regs.accumulator = (acc / num).to_le_bytes();
            },
            YoOp::Print => {
                let len = self.deref_usize(self.regs.ptr);
                let start = YoAddress(self.regs.ptr.0 + USIZE_SIZE);
                let string = self.mem.get_slice(start, len).unwrap();
                println!(">> {}", std::str::from_utf8(string).unwrap());
            },
            YoOp::PrintU32 => println!(">> Accumulator = {}", u32::from_le_bytes(self.regs.accumulator)),
            YoOp::PrintI32 => println!(">> Accumulator = {}", i32::from_le_bytes(self.regs.accumulator)),
            YoOp::ZeroAcc => self.regs.accumulator = [0; 4],
            YoOp::PushBasePtr => {
                let sp = self.mem.stack_head();
                self.mem.push_stack(USIZE_SIZE).unwrap(); 
                self.mem.write_bytes(sp, &self.regs.base_ptr.0.to_le_bytes()).unwrap();
                self.regs.base_ptr = self.mem.stack_head();
            },
            YoOp::PopBasePtr =>  {
                self.mem.set_stack_head(YoAddress(self.regs.base_ptr.0 - 8)).unwrap();
                self.regs.base_ptr = YoAddress(self.deref_usize(self.mem.stack_head()));
            },
            YoOp::LoadBasePtr => self.regs.ptr = self.regs.base_ptr,
            YoOp::LoadBasePtrRel => {
                let param = self.decode_usize_param().unwrap();
                self.regs.ptr = YoAddress(self.regs.base_ptr.0 + param);
            },
            YoOp::Not => for i in 0..self.regs.accumulator.len() {
                self.regs.accumulator[i] = !self.regs.accumulator[i];
            },
        }
    }

    pub fn exec_next(&mut self) -> YoExecProgress {
        match self.decode_next_op() {
            Some(op_info) => {
                self.exec(op_info.op);

                if let YoOp::Exit = op_info.op  {
                    YoExecProgress::Completed
                } else {
                    YoExecProgress::OnGoing
                }
            },
            None => YoExecProgress::Completed,
        }
    }

    pub fn exec_all(&mut self) {
       while let YoExecProgress::OnGoing = self.exec_next() { } 
    }
}
