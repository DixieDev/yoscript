START:
  pushBp

  ; Set number of iterations
  push 4
  loadBp
  store32 40 

  ; Call the func
  call FIBONACCI

  ; Clean-up reserved return value space
  pop 12

  ; Print the outcome
  printU32

  popBp
  exit


; fn FIBONACCI (iterations: u32) -> u32
FIBONACCI:
  ; Push base ptr and reserve stack space
  pushBp

  ; Reserve 4 bytes for the constant '1' (dont have db/dw/dd yet)
  ; Reserve 4 bytes for recursive call result
  ; Reserve another 4 bytes for recursive call param
  push 12

  ; Store 1 in the first slot
  loadBp
  store32 1

  load32 0      ; Load 0 into acc
  loadSpRel 32  ; Set ptr to point at iterations parameter (12 + 8 + 8 + 4)
  cmpU32De      ; Compare acc's value against the value at ptr destination

  jmpEq IS_FIRST_ITER

  ; else check if it's the second iter

  load32 1   
  cmpU32De    
  jmpEq IS_SECOND_ITER

  ;else handle other iters with recursive call

  ; Load iterations-1 into stack as parameter for func call
  deref32
  loadBp
  subU32De
  loadBpRel 8
  store32Acc

  ; Call FIBONACCI (iterations - 1)
  call FIBONACCI

  ; Store result in stack 
  loadBpRel 4
  store32Acc

  ; Load iterations-2 into stack as parameter for func call
  loadBpRel 8
  deref32
  loadBp
  subU32De
  loadBpRel 8
  store32Acc

  ; Call FIBONACCI (iterations - 2)
  call FIBONACCI

  ; Add the results together and return
  loadBpRel 4
  addU32De
  
  jmp FIB_END

IS_SECOND_ITER:
  load32 1
  jmp FIB_END

IS_FIRST_ITER:
  load32 0

FIB_END:
  popBp
  ret
