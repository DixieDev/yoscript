use yo::machine::mem::{ Megabytes, YoPartitionConfig };
use yo::machine::YoMachine;
use yo::asm::YoAssembler;
use std::time::Instant;

fn benchmark_yo(runs: u128) -> u128 {
    let mut machine = YoMachine::new(YoPartitionConfig {
        code_size:  Megabytes(3),
        stack_size: Megabytes(3),
        heap_size:  Megabytes(8),
    });

    let assembly = YoAssembler::assemble_file("./asm/test.asm");
    machine.mem.push_code(assembly.code()).unwrap();

    let mut total_millis = 0;
    for _ in 0..runs {
        let start = Instant::now();
        machine.exec_all();
        let end = Instant::now();
        total_millis += (end-start).as_millis();
        machine.reset_exec();
    }

    let average_millis = total_millis / runs;
    average_millis
}

fn fibonacci(iterations: u32) -> u32 {
    if iterations == 0 { 0 }
    else if iterations == 1 { 1 }
    else { fibonacci(iterations-1) + fibonacci(iterations-2) }
}

fn benchmark_rust(runs: u128) -> u128 {
    let mut total_millis = 0;
    for _ in 0..runs {
        let start = Instant::now();
        let res = fibonacci(40);
        let end = Instant::now();
        println!("{}", res);
        total_millis += (end-start).as_millis();
    }

    let average_millis = total_millis / runs;
    average_millis
}

fn main() {
    let num_runs = 10;
    let yo_time = benchmark_yo(num_runs);
    let rust_time = benchmark_rust(num_runs);

    println!("YoVM took {} milliseconds on average to compute the 40th fibonacci number recursively", yo_time);
    println!("Rust took {} milliseconds on average to compute the 40th fibonacci number recursively", rust_time);
    println!("Rust is approx {} times faster than Yo", yo_time / rust_time);
}

