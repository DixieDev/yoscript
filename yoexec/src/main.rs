use yo::machine::mem::{ Megabytes, YoPartitionConfig };
use yo::machine::YoMachine;
use std::io::Read;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Incorrect number of args! Usage: yoexec [bytecode_file]");
        return;
    }

    let mut file = std::fs::File::open(&args[1]).unwrap();
    let mut code: Vec<u8> = Vec::new();
    file.read_to_end(&mut code).unwrap();
    
    let mut machine = YoMachine::new(YoPartitionConfig {
        code_size:  Megabytes(3),
        stack_size: Megabytes(3),
        heap_size:  Megabytes(8),
    });

    machine.mem.push_code(&code).unwrap();
    machine.exec_all();

    machine.mem.print_stack_range(0..32);
    println!("\n{:?}", machine);

}

