pub const USIZE_SIZE: usize = std::mem::size_of::<usize>();

macro_rules! ops {
    ($($op:ident $param:ident: $desc:expr,)*) => {
        // Declare big array of op info
        pub static OPS: &[OpInfo] = &[
            OpInfo {
                op: YoOp::NoOp,
                param: OpParam::None,
            },
            $(
                OpInfo {
                    op: YoOp::$op,
                    param: OpParam::$param,
                },
            )+
        ];

        #[repr(u8)]
        #[derive(Copy, Clone, Debug)]
        pub enum YoOp {
            NoOp = 0,
            $($op,)+
        }

        impl YoOp {
            pub fn info(self) -> OpInfo {
                OPS[self as usize]
            }

            pub fn full_size(self) -> usize {
                1 + OPS[self as usize].param.size()
            }

            #[cfg(test)]
            pub fn csv() -> String {
                let mut out = "Byte, Name, Param, Description\n".to_owned();

                let mut bc = 0;
                out.push_str(&format!("{:#04x}, {}, {}, {}\n", bc, "NoOp", "None", "Does nothing"));

                $(
                    bc += 1;
                    out.push_str(&format!("{:#04x}, {}, {}, {}\n", bc, stringify!($op), stringify!($param), $desc));
                )+

                out
            }

            #[cfg(test)]
            pub fn cpp() -> String {
                let mut out = "enum class Opcode : uint8_t {\n\tNoOp = 0x00,\n".to_string();

                let mut bc = 0;
                $(
                    bc += 1;
                    out.push_str(&format!("\t{} = {:#04x},\n", stringify!($op), bc));
                )+

                out.push_str("};");

                out
            }
        }
    }
}

ops![
    Exit None: "Stops execution of the program",

    Push Usize: "Pushes the specified number of bytes onto the stack",
    Pop Usize: "Pops the specified number of bytes off of the stack",

    Alloc Usize: "Allocates the specified number of bytes on the heap and stores the address in the ptr register",
    Free Usize: "Frees memory that's been dynamically allocated at the address stored in the ptr register",

    Store8 U8: "Stores the 8-bit parameter at the ptr register's destination",
    Store32 U32: "Stores the 32-bit parameter at the ptr register's destination",
    StoreUsize Usize: "Stores the usize parameter at the ptr register's destination",
    StoreAcc8 None: "Stores the acc register's least-significant byte at the ptr register's destination",
    StoreAcc32 None: "Stores the acc register's value at the ptr register's destination",

    Load8 U8: "Loads the 8-bit parameter into the acc register's last byte",
    Load32 U32: "Loads the 32-bit parameter into the acc register",
    Load8De None: "Dereferences a byte from the ptr register's destination and loads it into the acc register's least-significant byte",
    Load32De None: "Dereferences 4 bytes from the ptr register's destination and loads them into the acc register",

    LoadPtr Usize: "Loads the usize parameter into the ptr register",
    LoadPtrDe None: "Dereferences a usize at the ptr register's destination and loads it into the ptr register",
    LoadStackPtr None: "Loads the stack pointer into the ptr register",
    LoadStackPtrRel Usize: "Loads the usize parameter subtracted from the stack pointer into the ptr register",

    AddPtr Usize: "Adds the usize parameter to the ptr register's current value",
    SubPtr Usize: "Subtracts the usize parameter from the ptr register's current value",

    CmpU8 U8: "Compares the u8 parameter against the acc register's least-significant byte",
    CmpU32 U32: "Compares the u32 parameter against the acc register's value",
    CmpI8 I8: "Compares the i8 parameter against the acc register's least-significant byte",
    CmpI32 I32: "Compares the i32 parameter against the acc register's value",

    CmpU8De None: "Compares the u8 value at the ptr register's address against the acc register's least-significant byte",
    CmpU32De None: "Compares the u32 value at the ptr register's address against the acc register's value",
    CmpI8De None: "Compares the i8 value at the ptr register's address against the acc register's least-significant byte", 
    CmpI32De None: "Compares the i32 value at the ptr register's address against the acc register's value",

    CmpPtr Usize: "Compares the usize parameter against the ptr register's value",
    CmpPtrDe Usize: "Compares the usize parameter against the ptr register's destination",
    
    Jump Usize: "Jumps execution to the specified address",
    JumpEq Usize: "Jumps execution to the specified address if the eq flag is set",
    JumpNe Usize: "Jumps execution to the specified address if the eq flag is not set",
    JumpGt Usize: "Jumps execution to the specified address if the gt flag is set",
    JumpLt Usize: "Jumps execution to the specified address if the lt flag is set",
    JumpGe Usize: "Jumps execution to the specified address if the eq or gt flags are set",
    JumpLe Usize: "Jumps execution to the specified address if the eq or lt flags are set",

    Call Usize: "Pushes the next instruction's address onto the stack and jumps to the specified address",
    CallPtr None: "Pushes the next instruction's address onto the stack and jumps to the ptr register's address",
    Return None: "Pops an address off the stack and jumps to it",

    AddU8De None: "Adds the u8 value at the ptr register's destination to the acc register's least-significant bit",
    AddU32De None: "Adds the u32 value at the ptr register's destination to the acc register's value",
    AddI8De None: "Adds the i8 value at the ptr register's destination to the acc register's least-significant bit",
    AddI32De None: "Adds the i32 value at the ptr register's destination to the acc register's value",

    SubU8De None: "Subtracts the u8 value at the ptr register's destination from the acc register's least-significant bit",
    SubU32De None: "Subtracts the u32 value at the ptr register's destination from the acc register's value",
    SubI8De None: "Subtracts the i8 value at the ptr register's destination from the acc register's least-significant bit",
    SubI32De None: "Subtracts the i32 value at the ptr register's destination from the acc register's value",

    MulU8De None: "Multiplies the acc register's least-significant bit by the u8 value at the ptr register's destination",
    MulU32De None: "Multiplies the acc register's value by the u32 value at the ptr register's destination",
    MulI8De None: "Multiplies the acc register's least-significant bit by the i8 value at the ptr register's destination",
    MulI32De None: "Multiplies the acc register's value by the i32 value at the ptr register's destination",

    DivU8De None: "Divides the acc register's least-significant bit by the u8 value at the ptr register's destination",
    DivU32De None: "Divides the acc register's value by the u32 value at the ptr register's destination",
    DivI8De None: "Divides the acc register's least-significant bit by the i8 value at the ptr register's destination",
    DivI32De None: "Divides the acc register's value by the i32 value at the ptr register's destination",

    Print None: "Outputs the utf-8 string at the ptr register's destination to stdout. The string must begin with a usize value that specifies its length in bytes",
    PrintU32 None: "Outputs the acc register's u32 value to stdout on a new line",
    PrintI32 None: "Outputs the acc register's i32 value to stdout on a new line",

    ZeroAcc None: "Set the acc register's value to zero",

    PushBasePtr None: "Pushes the base pointer to the stack and then sets the base pointer register to the stack pointer's current value",
    PopBasePtr None: "Pops the base pointer off of the stack and stores it in the base pointer register",
    LoadBasePtr None: "Loads the base pointer into the ptr register",
    LoadBasePtrRel Usize: "Loads the usize parameter added onto the base pointer into the ptr register",

    Not None: "Flips the bits of the acc register's value",
];

#[derive(Copy, Clone, Debug)]
pub struct OpInfo {
    pub op: YoOp,
    pub param: OpParam,
}

#[derive(Copy, Clone, Debug)]
pub enum OpParam {
    None,
    U8,
    U32,
    Usize,
    I8,
    I32,
}

impl OpParam {
    pub fn size(self) -> usize {
        match self {
            Self::None => 0,
            Self::U8 => 1,
            Self::U32 => 4,
            Self::Usize => USIZE_SIZE,
            Self::I8 => 1,
            Self::I32 => 4,
        }
    }
}

pub struct OpEncoder { }

impl OpEncoder {
    #[inline]
    fn op_and_usize(op: YoOp, num: usize) -> [u8; 1+USIZE_SIZE] {
        let mut out = [0; 1+USIZE_SIZE];
        out[0] = op as u8;
        out[1..].copy_from_slice(&num.to_le_bytes());
        out
    }

    pub fn no_op() -> [u8; 1] {
        [YoOp::NoOp as u8]
    }

    pub fn exit() -> [u8; 1] {
        [YoOp::Exit as u8]
    }

    pub fn push(push_len: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::Push, push_len)
    }

    pub fn pop(pop_len: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::Pop, pop_len)
    }

    pub fn alloc(alloc_len: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::Alloc, alloc_len)
    }

    pub fn free(free_ref: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::Free, free_ref)
    }

    pub fn store_8(byte: u8) -> [u8; 2] {
        [YoOp::Store8 as u8, byte]
    }

    pub fn store_32(num: u32) -> [u8; 5] {
        let mut out = [0; 5];
        out[0] = YoOp::Store32 as u8;
        out[1..].copy_from_slice(&num.to_le_bytes());
        out
    }

    pub fn store_usize(num: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::StoreUsize, num)
    }

    pub fn load_acc_8(byte: u8) -> [u8; 2] {
        [YoOp::Load8 as u8, byte]
    }

    pub fn load_acc_32(num: u32) -> [u8; 5] {
        let mut out = [0; 5];
        out[0] = YoOp::Load32 as u8;
        out[1..].copy_from_slice(&num.to_le_bytes());
        out
    }

    pub fn load_acc_8_de() -> [u8; 1] {
        [YoOp::Load8De as u8]
    }

    pub fn load_acc_32_de() -> [u8; 1] {
        [YoOp::Load32De as u8]
    }

    pub fn load_ptr(address: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::LoadPtr, address)
    }

    pub fn load_ptr_deref() -> [u8; 1] {
        [YoOp::LoadPtrDe as u8]
    }

    pub fn load_stack_ptr() -> [u8; 1] {
        [YoOp::LoadStackPtr as u8]
    }

    pub fn load_stack_ptr_relative(offset: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::LoadStackPtrRel, offset)
    }

    pub fn add_ptr(increase: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::AddPtr, increase)
    }

    pub fn sub_ptr(decrease: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::SubPtr, decrease)
    }

    pub fn store_acc_8() -> [u8; 1] {
        [YoOp::StoreAcc8 as u8]
    }

    pub fn store_acc_32() -> [u8; 1] {
        [YoOp::StoreAcc32 as u8]
    }

    pub fn cmp_u8(num: u8) -> [u8; 2] {
        [YoOp::CmpU8 as u8, num]
    }

    pub fn cmp_u32(num: u32) -> [u8; 5] {
        let mut out = [0; 5];
        out[0] = YoOp::CmpU32 as u8;
        out[1..].copy_from_slice(&num.to_le_bytes());
        out
    }

    pub fn cmp_i8(num: i8) -> [u8; 2] {
        [YoOp::CmpI8 as u8, num.to_le_bytes()[0]]
    }

    pub fn cmp_i32(num: i32) -> [u8; 5] {
        let mut out = [0; 5];
        out[0] = YoOp::CmpI32 as u8;
        out[1..].copy_from_slice(&num.to_le_bytes());
        out
    }

    pub fn cmp_u8_de() -> [u8; 1] {
        [YoOp::CmpU8De as u8]
    }

    pub fn cmp_u32_de() -> [u8; 1] {
        [YoOp::CmpU32De as u8]
    }

    pub fn cmp_i8_de() -> [u8; 1] {
        [YoOp::CmpI8De as u8]
    }

    pub fn cmp_i32_de() -> [u8; 1] {
        [YoOp::CmpI32De as u8]
    }

    pub fn cmp_ptr(ptr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::CmpPtr, ptr)
    }

    pub fn cmp_ptr_de(ptr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::CmpPtrDe, ptr)
    }

    pub fn jmp(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::Jump, addr)
    }

    pub fn jmp_eq(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::JumpEq, addr)
    }

    pub fn jmp_ne(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::JumpNe, addr)
    }

    pub fn jmp_gt(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::JumpGt, addr)
    }

    pub fn jmp_lt(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::JumpLt, addr)
    }

    pub fn jmp_ge(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::JumpGe, addr)
    }

    pub fn jmp_le(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::JumpLe, addr)
    }

    pub fn call(addr: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::Call, addr)
    }

    pub fn call_ptr() -> [u8; 1] {
        [YoOp::CallPtr as u8]
    }

    pub fn ret() -> [u8; 1] {
        [YoOp::Return as u8]
    }

    pub fn add_u8_de() -> [u8; 1] {
        [YoOp::AddU8De as u8]
    }

    pub fn add_u32_de() -> [u8; 1] {
        [YoOp::AddU32De as u8]
    }

    pub fn add_i8_de() -> [u8; 1] {
        [YoOp::AddI8De as u8]
    }

    pub fn add_i32_de() -> [u8; 1] {
        [YoOp::AddI32De as u8]
    }

    pub fn sub_u8_de() -> [u8; 1] {
        [YoOp::SubU8De as u8]
    }

    pub fn sub_u32_de() -> [u8; 1] {
        [YoOp::SubU32De as u8]
    }

    pub fn sub_i8_de() -> [u8; 1] {
        [YoOp::SubI8De as u8]
    }

    pub fn sub_i32_de() -> [u8; 1] {
        [YoOp::SubI32De as u8]
    }

    pub fn mul_u8_de() -> [u8; 1] {
        [YoOp::MulU8De as u8]
    }

    pub fn mul_u32_de() -> [u8; 1] {
        [YoOp::MulU32De as u8]
    }

    pub fn mul_i8_de() -> [u8; 1] {
        [YoOp::MulI8De as u8]
    }

    pub fn mul_i32_de() -> [u8; 1] {
        [YoOp::MulI32De as u8]
    }

    pub fn div_u8_de() -> [u8; 1] {
        [YoOp::DivU8De as u8]
    }

    pub fn div_u32_de() -> [u8; 1] {
        [YoOp::DivU32De as u8]
    }

    pub fn div_i8_de() -> [u8; 1] {
        [YoOp::DivI8De as u8]
    }

    pub fn div_i32_de() -> [u8; 1] {
        [YoOp::DivI32De as u8]
    }

    pub fn print() -> [u8; 1] {
        [YoOp::Print as u8]
    }

    pub fn print_u32() -> [u8; 1] {
        [YoOp::PrintU32 as u8]
    }

    pub fn print_i32() -> [u8; 1] {
        [YoOp::PrintI32 as u8]
    }

    pub fn zero_acc() -> [u8; 1] {
        [YoOp::ZeroAcc as u8]
    }

    pub fn push_base_ptr() -> [u8; 1] {
        [YoOp::PushBasePtr as u8]
    }

    pub fn pop_base_ptr() -> [u8; 1] {
        [YoOp::PopBasePtr as u8]
    }

    pub fn load_base_ptr() -> [u8; 1] {
        [YoOp::LoadBasePtr as u8]
    }

    pub fn load_base_ptr_rel(offset: usize) -> [u8; 1+USIZE_SIZE] {
        Self::op_and_usize(YoOp::LoadBasePtrRel, offset)
    }

    pub fn not() -> [u8; 1] {
        [YoOp::Not as u8]
    }
}

#[cfg(test)]
mod tests {
    use crate::ops::YoOp;
    use std::io::Write;

    #[test]
    fn write_csv() {
        let mut file = std::fs::File::create("yoscript.csv").unwrap();
        file.write_all(YoOp::csv().as_bytes()).unwrap();
    }

    #[test]
    fn write_cpp() {
        let mut file = std::fs::File::create("opcodes.cpp").unwrap();
        file.write_all(YoOp::cpp().as_bytes()).unwrap();
    }
}
